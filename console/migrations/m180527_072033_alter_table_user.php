<?php

use yii\db\Migration;

/**
 * Class m180527_072033_alter_table_user
 */
class m180527_072033_alter_table_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            'user', //table name 
            'user_type', // column name
            $this->smallInteger()
                ->unique()
                ->notNull()
                ->defaultValue(1)
                ->after('email')
                ->comment('User type. Default 1 as Admin.')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180527_072033_alter_table_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180527_072033_alter_table_user cannot be reverted.\n";

        return false;
    }
    */
}
