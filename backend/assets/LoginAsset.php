<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class LoginAsset extends AssetBundle
{
    public $sourcePath = '@bower/admin-lte/';

    public $css = [
        'bootstrap/css/bootstrap.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css',
        'dist/css/AdminLTE.css',
        'plugins/iCheck/square/blue.css'
    ];

    public $js = [
        'plugins/iCheck/icheck.min.js',
        // 'plugins/jQuery/jquery-2.2.3.min.js',
        // 'bootstrap/js/bootstrap.min.js',
        // 'plugins/slimScroll/jquery.slimscroll.min.js',
        // 'plugins/fastclick/fastclick.js',
        // 'dist/js/app.min.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

    /* ORIGIN
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ]; 
    */
}
