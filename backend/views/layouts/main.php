<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;

$asset = AppAsset::register($this);
$baseUrl = $asset->baseUrl;
?>


<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
    
<div class="wrapper">

    <?=$this->render('main_header', ['asset'=> $asset, 'baseUrl'=> $baseUrl])?>

    <?=$this->render('main_sidebar', ['asset'=> $asset, 'baseUrl'=> $baseUrl])?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <?=$this->render('main_content_header', ['asset'=> $asset, 'baseUrl'=> $baseUrl])?>

    <!-- Main content -->
    <section class="content">
      
    <?php $this->beginBody() ?>
        <?= $content ?>   
    <?php $this->endBody() ?>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.8
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>    

</div>


    
</body>
</html>


<?php $this->endPage() ?>
